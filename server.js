const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000; // Puerto de la aplicación

app.use(express.json());

app.use('/api/', require('./routes/routes')); // Importa y usa las rutas del directorio routes

// Manejador de ruta de inicio
app.get('/', (req, res) => {
  res.send('¡Bienvenido a mi API!');
});

// Manejador de errores para rutas no encontradas
app.use((req, res, next) => {
  res.status(404).json({ error: 'Ruta no encontrada' });
});

// Manejador de errores del servidor
app.use((err, req, res, next) => {
  console.error(err);
  res.status(500).json({ error: 'Error del servidor' });
});

// Iniciar el servidor
app.listen(PORT, () => {
  console.log('Servidor ejecutándose en puerto', PORT);
});
