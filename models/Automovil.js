const mongoose = require('mongoose');

const automovilSchema = new mongoose.Schema({
    id: {
        type: String,
        unique: true,
        required: true
    },
    marca: {
        type: String,
        required: true
    },
    año: {
        type: Number,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    turbo: {
        type: Boolean,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    motor: {
        type: String,
        required: true
    },
    cabinas: {
        type: Number,
        required: false
    },
    sunroof: {
        type: Boolean,
        required: false
    },
    popularidad: {
        type: Number,
        required: true,
        default: 0 // Valor inicial de popularidad
    }
});

const Automovil = mongoose.model('Automovil', automovilSchema);

module.exports = Automovil;
