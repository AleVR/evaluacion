const express = require('express');
const router = express.Router();
const automovilesController = require('../controllers/automovilesController');

// Ruta para generar automóviles
router.post('/generar-automoviles', automovilesController.generarAutomoviles);

// Ruta para filtrar automóviles
router.get('/filtrar-automoviles', automovilesController.filtrarAutomoviles);

// Ruta para obtener un automovil por ID
router.get('/:id', automovilesController.obtenerAutomovilPorId);

module.exports = router;