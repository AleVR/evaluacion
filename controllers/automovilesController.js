const { generarAutomovil, guardarAutomoviles } = require('../services/generarAutomovilService');
const { filtrarAutomovil } = require('../services/filtrarAutomovilService');
const { consultarAutomovil } = require('../services/consultarAutomovilService');
const automovilesJSON = require('../automoviles.json');

// Función para generar N cantidad de automóviles
const generarAutomoviles = (req, res) => {
    const cantidad = req.body.cantidad;

    const automovilesGenerados = [];

    for (let i = 0; i < cantidad; i++) {
        const automovil = generarAutomovil();
        automovilesGenerados.push(automovil);
    }

    guardarAutomoviles(automovilesGenerados);

    res.status(200).json({ automoviles: automovilesGenerados });
};

// Función para filtrar automóviles
const filtrarAutomoviles = (req, res) => {
    const { precio, tipo, color } = req.query;

    const automovilesFiltrados = filtrarAutomovil(automovilesJSON, precio, tipo, color);

    res.json({ automoviles: automovilesFiltrados });
};

// Función para obtener un automovil por ID
const obtenerAutomovilPorId = (req, res) => {
    const { id } = req.params;
    const automovil = consultarAutomovil(automovilesJSON, id);

    if (!automovil) {
        return res.status(404).json({ error: 'Automovil no encontrado' });
    }

    automovil.popularidad++;

    res.json(automovil);
};

module.exports = { generarAutomoviles, filtrarAutomoviles, obtenerAutomovilPorId };
