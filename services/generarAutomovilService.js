const fs = require('fs');

// Lógica para generar un automóvil aleatorio
const generarAutomovil = () => {
    const marcas = ['Toyota', 'Honda', 'Chevrolet', 'Ford', 'Nissan'];
    const anios = [2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023];
    const colores = ['Rojo', 'Azul', 'Negro', 'Blanco', 'Gris'];
    const precios = [8000000, 10000000, 15000000, 20000000, 25000000, 30000000];
    const turbos = ['Si', 'No'];
    const tipos = ['Sedan', 'Camioneta', 'SUV'];
    const motoresSedan = ['1.4cc', '1.6cc', '2.0cc'];
    const motoresCamioneta = ['2.4cc', '3.0cc', '4.0cc'];
    const motoresSUV = ['1.8cc', '2.2cc', '2.8cc'];
    const cabinasCamioneta = ['1', '2'];
    const sunroofsSUV = ['Si', 'No'];

    const marca = marcas[Math.floor(Math.random() * marcas.length)];
    const anio = anios[Math.floor(Math.random() * anios.length)];
    const color = colores[Math.floor(Math.random() * colores.length)];
    const precio = precios[Math.floor(Math.random() * precios.length)];
    const turbo = turbos[Math.floor(Math.random() * turbos.length)];
    const tipo = tipos[Math.floor(Math.random() * tipos.length)];
    const popularidad = 0;
    let motor;
    let cabinas;
    let sunroof;

    switch (tipo) {
        case 'Sedan':
            motor = motoresSedan[Math.floor(Math.random() * motoresSedan.length)];
            cabinas = null;
            sunroof = null;
            break;
        case 'Camioneta':
            motor = motoresCamioneta[Math.floor(Math.random() * motoresCamioneta.length)];
            cabinas = cabinasCamioneta[Math.floor(Math.random() * cabinasCamioneta.length)];
            sunroof = null;
            break;
        case 'SUV':
            motor = motoresSUV[Math.floor(Math.random() * motoresSUV.length)];
            cabinas = null;
            sunroof = sunroofsSUV[Math.floor(Math.random() * sunroofsSUV.length)];
            break;
        default:
            motor = null;
            cabinas = null;
            sunroof = null;
    }

    const automovil = {
        id: generateUniqueId(), // Función para generar ID único
        marca: marca,
        anio: anio,
        color: color,
        precio: precio,
        turbo: turbo,
        tipo: tipo,
        motor: motor,
        cabinas: cabinas,
        sunroof: sunroof,
        popularidad: popularidad,
    };

    return automovil;
};

// Función para generar un ID único
const generateUniqueId = () => {
    return Math.random().toString(36).substring(2) + Date.now().toString(36);
};

// Función para guardar los automóviles en un archivo JSON
const guardarAutomoviles = (automoviles) => {
  const automovilesJSON = JSON.stringify(automoviles);
  fs.writeFileSync('automoviles.json', automovilesJSON, 'utf8');
};

module.exports = { generarAutomovil, guardarAutomoviles };
