// Lógica para consultar un automovil por ID
const consultarAutomovil = (automoviles, id) => {
    return automoviles.find(automovil => automovil.id === id);
};

module.exports = { consultarAutomovil };
