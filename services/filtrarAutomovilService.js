// Lógica para filtrar los automóviles según los filtros especificados
const filtrarAutomovil = (automoviles, precio, tipo, color) => {
    let automovilesFiltrados = automoviles;
  
    // Filtrar por precio
    if (precio) {
      automovilesFiltrados = automovilesFiltrados.filter(automovil => automovil.precio <= precio);
    }
  
    // Filtrar por tipo
    if (tipo) {
      automovilesFiltrados = automovilesFiltrados.filter(automovil => automovil.tipo.toLowerCase() === tipo.toLowerCase());
    }
  
    // Filtrar por color
    if (color) {
      automovilesFiltrados = automovilesFiltrados.filter(automovil => automovil.color.toLowerCase() === color.toLowerCase());
    }
  
    return automovilesFiltrados;
};

module.exports = { filtrarAutomovil };
